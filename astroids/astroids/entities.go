package astroids

import (
	"math"

	"github.com/veandco/go-sdl2/sdl"
)

// Cheap entity system
type Entity struct {
	xy    Position
	dxy   Position
	angle float64
	ani   Animation
	life  int
}

func (e *Entity) Draw(renderer *sdl.Renderer, textures map[string]*sdl.Texture) {
	frame := &e.ani.frames[int(e.ani.frameIndex)]
	frameOrigin := sdl.FPoint{X: float32(frame.W) / 2, Y: float32(frame.H) / 2}

	renderer.CopyExF(textures[e.ani.textureId],
		frame,
		&sdl.FRect{X: e.xy.X, Y: e.xy.Y, W: float32(frame.W), H: float32(frame.H)},
		e.angle,
		&frameOrigin,
		sdl.FLIP_NONE)
}

func (e *Entity) GetPosition() Position {
	return e.xy
}
func (e *Entity) GetOrigin() Position {
	frame := &e.ani.frames[int(e.ani.frameIndex)]
	return Position{X: e.xy.X + float32(frame.W)/2, Y: e.xy.Y + float32(frame.H)/2}
}

func (e *Entity) GetDegree() float64 {
	return e.angle
}
func (e *Entity) GetLife() int {
	return e.life
}
func (e *Entity) IsAlive() bool {
	return e.GetLife() > 0
}
func (e *Entity) ApplyLife(val int) {
	e.life += val
}

func (e *Entity) CollidesWith(other *Entity) bool {

	var x = e.xy.X
	var y = e.xy.Y
	var w = float32(e.ani.frames[int(e.ani.frameIndex)].W)
	var h = float32(e.ani.frames[int(e.ani.frameIndex)].H)

	var otherX = other.xy.X
	var otherY = other.xy.Y
	var otherW = float32(other.ani.frames[int(other.ani.frameIndex)].W)
	var otherH = float32(other.ani.frames[int(other.ani.frameIndex)].H)

	return x+w > otherX && x < otherX+otherW && y+h > otherY && y < otherY+otherH

}

type IEntity interface {
	Update(delta float32)
	Draw(renderer *sdl.Renderer, textures map[string]*sdl.Texture)
	IsAlive() bool
	ApplyLife(val int)
}

//
//Spaceship
//
type Spaceship struct {
	Ent Entity
}

func (s *Spaceship) Reset(xpos float32, ypos float32) {
	s.Ent.xy.X = xpos
	s.Ent.xy.Y = ypos
	s.Ent.life = 100
}

func (s *Spaceship) IsAlive() bool {
	return s.Ent.IsAlive()
}
func (s *Spaceship) ApplyLife(val int) {
	s.Ent.ApplyLife(val)
}

func (s *Spaceship) Draw(renderer *sdl.Renderer, textures map[string]*sdl.Texture) {
	s.Ent.Draw(renderer, textures)
}

func (s *Spaceship) Update(delta float32) {
	//probably a bad place to put this
	entity := &s.Ent
	thrust := false
	keyboardState := sdl.GetKeyboardState()
	if keyboardState[sdl.SCANCODE_UP] == 1 {
		thrust = true
	}
	if keyboardState[sdl.SCANCODE_LEFT] == 1 {
		entity.angle += -2.0
	}
	if keyboardState[sdl.SCANCODE_RIGHT] == 1 {
		entity.angle += 2.0
	}

	const maxSpeed = 3.0
	if thrust {
		entity.dxy.X += float32(math.Sin(entity.angle*(math.Pi/180))) * delta * maxSpeed
		entity.dxy.Y -= float32(math.Cos(entity.angle*(math.Pi/180))) * delta * maxSpeed

	} else {
		entity.dxy.X *= .99
		entity.dxy.Y *= .99
	}
	speed := math.Sqrt(float64(entity.dxy.X*entity.dxy.X + entity.dxy.Y*entity.dxy.Y))
	if speed > maxSpeed {
		entity.dxy.X *= float32(maxSpeed / speed)
		entity.dxy.Y *= float32(maxSpeed / speed)
	}

	entity.xy.X += entity.dxy.X
	entity.xy.Y += entity.dxy.Y

	W := float32(ScreenWidth)
	H := float32(ScreenHeight)
	entity.xy.Wrap(0, W, 0, H)
	entity.ani.Update(delta)

	// overriding the behavior of 'ani.Update'
	if !thrust {
		entity.ani.SetFrameIndex(0)
	} else {
		if entity.ani.frameIndex < 1 {
			entity.ani.SetFrameIndex(1)
		}
	}
}

func NewSpaceShipEntity(xpos float32, ypos float32) *Spaceship {
	return &Spaceship{
		Ent: Entity{
			xy: Position{
				X: xpos,
				Y: ypos,
			},
			dxy: Position{
				X: 0,
				Y: 0,
			},
			angle: 0,
			ani:   MakeAnimation(SPACESHIP, sdl.Rect{X: 0, Y: 0, W: 41, H: 45}, 3, 10.0),
			life:  100,
		},
	}
}

//
// Bullet
//
type Bullet struct {
	Ent Entity
}

func (e *Bullet) IsAlive() bool {
	return e.Ent.IsAlive()
}
func (e *Bullet) ApplyLife(val int) {
	e.Ent.ApplyLife(val)
}

func (e *Bullet) Draw(renderer *sdl.Renderer, textures map[string]*sdl.Texture) {
	e.Ent.Draw(renderer, textures)
}

func (e *Bullet) Update(delta float32) {
	entity := &e.Ent
	const maxSpeed = 8.0
	entity.dxy.X += float32(math.Sin(entity.angle*(math.Pi/180))) * delta * maxSpeed
	entity.dxy.Y -= float32(math.Cos(entity.angle*(math.Pi/180))) * delta * maxSpeed
	speed := math.Sqrt(float64(entity.dxy.X*entity.dxy.X + entity.dxy.Y*entity.dxy.Y))
	if speed > maxSpeed {
		entity.dxy.X *= float32(maxSpeed / speed)
		entity.dxy.Y *= float32(maxSpeed / speed)
	}

	entity.xy.X += entity.dxy.X
	entity.xy.Y += entity.dxy.Y

	W := float32(ScreenWidth)
	H := float32(ScreenHeight)
	if entity.xy.X > W || entity.xy.X < 0 || entity.xy.Y > H || entity.xy.Y < 0 {
		entity.life = 0
	}
	entity.ani.Update(delta)
}

//https://math.stackexchange.com/questions/143932/calculate-point-given-x-y-angle-and-distance
func NewBulletEntity(xpos float32, ypos float32, angle float64) *Bullet {
	return &Bullet{
		Ent: Entity{
			xy: Position{
				X: xpos,
				Y: ypos,
			},
			dxy: Position{
				X: 0,
				Y: 0,
			},
			angle: angle,
			ani:   MakeAnimation(BULLET, sdl.Rect{X: 0, Y: 0, W: 5, H: 11}, 2, 15.0),
			life:  100,
		},
	}
}

//
// Rock
//
type Rock struct {
	Ent Entity
}

func (s *Rock) IsAlive() bool {
	return s.Ent.IsAlive()
}

func (e *Rock) ApplyLife(val int) {
	e.Ent.ApplyLife(val)
}

func (r *Rock) Draw(renderer *sdl.Renderer, textures map[string]*sdl.Texture) {
	r.Ent.Draw(renderer, textures)
}

func (r *Rock) Update(delta float32) {
	entity := &r.Ent
	var maxSpeed float32 = 20.0
	entity.xy.X += entity.dxy.X * delta * maxSpeed
	entity.xy.Y += entity.dxy.Y * delta * maxSpeed
	W := float32(ScreenWidth)
	H := float32(ScreenHeight)
	entity.xy.Wrap(0, W, 0, H)

	entity.ani.Update(delta)
}
func NewRockEntity(xpos float32, ypos float32, dxpos float32, dypos float32, angle float64) *Rock {
	return &Rock{
		Ent: Entity{
			xy: Position{
				X: xpos,
				Y: ypos,
			},
			dxy: Position{
				X: dxpos,
				Y: dypos,
			},
			angle: angle,
			ani:   MakeAnimation(ROCK, sdl.Rect{X: 0, Y: 0, W: 64, H: 64}, 16, 20.0),
			life:  100,
		},
	}
}

//
// Rock
//
type Explosion struct {
	Ent Entity
}

func (e *Explosion) IsAlive() bool {
	return e.Ent.IsAlive()
}

func (e *Explosion) ApplyLife(val int) {
	e.Ent.ApplyLife(val)
}

func (e *Explosion) Draw(renderer *sdl.Renderer, textures map[string]*sdl.Texture) {
	e.Ent.Draw(renderer, textures)
}

func (e *Explosion) Update(delta float32) {
	entity := &e.Ent
	entity.xy.X += entity.dxy.X
	entity.xy.Y += entity.dxy.Y

	W := float32(ScreenWidth)
	H := float32(ScreenHeight)
	entity.xy.Wrap(0, W, 0, H)

	var wasreset = entity.ani.Update(delta)
	if wasreset {
		entity.life = 0
	}
}
func NewExplosionEntity(xpos float32, ypos float32, dxpos float32, dypos float32, angle float64) *Explosion {
	return &Explosion{
		Ent: Entity{
			xy: Position{
				X: xpos,
				Y: ypos,
			},
			dxy: Position{
				X: dxpos,
				Y: dypos,
			},
			angle: angle,
			ani:   MakeAnimation(EXPLOSION, sdl.Rect{X: 0, Y: 0, W: 192, H: 192}, 64, 40.0),
			life:  100,
		},
	}
}
