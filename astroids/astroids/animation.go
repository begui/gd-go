package astroids

import (
	"github.com/veandco/go-sdl2/sdl"
)

type Animation struct {
	textureId  string
	speed      float32
	frameIndex float32
	frames     []sdl.Rect
}

func (ani *Animation) Update(delta float32) bool {
	ani.frameIndex += delta * ani.speed

	if ani.frameIndex >= float32(len(ani.frames)) {
		ani.frameIndex = 0
	}
	return ani.frameIndex == 0
}
func (ani *Animation) SetFrameIndex(index float32) {
	if index < float32(len(ani.frames)) {
		ani.frameIndex = index
	}
}

func MakeAnimation(textureId string, frame sdl.Rect, frameCount int32, speed float32) Animation {

	framebuilder := func(frame *sdl.Rect, frameCount int32) []sdl.Rect {
		frames := make([]sdl.Rect, 0, frameCount)
		for i := int32(0); i < frameCount; i++ {
			x := frame.X + frame.W*i
			y := frame.Y
			w := frame.W
			h := frame.H
			frames = append(frames, sdl.Rect{X: x, Y: y, W: w, H: h})
		}
		return frames
	}

	ani := Animation{
		textureId:  textureId,
		speed:      speed,
		frameIndex: 0,
		frames:     framebuilder(&frame, frameCount),
	}
	return ani
}
