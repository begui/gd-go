package astroids

const ScreenTitle = "Astroids"
const ScreenWidth, ScreenHeight int32 = 800, 600

const GD_ASSETS_PATH = "assets/"
const (
	ROCK      = "img/rock.png"
	SPACESHIP = "img/playerspaceship.png"
	BULLET    = "img/bullet.png"
	EXPLOSION = "img/explosion.png"
)
