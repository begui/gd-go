package astroids

type Position struct {
	X float32
	Y float32
}

func (p *Position) Wrap(minXpos float32, maxXpos float32, minYpos float32, maxYpos float32) {

	if p.X > maxXpos {
		p.X = minXpos
	}
	if p.X < minXpos {
		p.X = maxXpos
	}
	if p.Y > maxYpos {
		p.Y = minYpos
	}
	if p.Y < minYpos {
		p.Y = maxYpos
	}

}
