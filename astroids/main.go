package main

import (
	"container/list"
	"gd-go/astroids/astroids"
	"math"
	"math/rand"
	"time"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

// This can be moved to a utility package
func LoadTexture(renderer *sdl.Renderer, filename string) *sdl.Texture {

	image, err := img.Load(filename)
	if err != nil {
		panic(err)
	}
	defer image.Free()

	texture, err := renderer.CreateTextureFromSurface(image)
	if err != nil {
		panic(err)
	}

	return texture
}

const screenWidth, screenHeight = astroids.ScreenWidth, astroids.ScreenHeight

func main() {
	rand.Seed(time.Now().UnixNano())

	var textures = make(map[string]*sdl.Texture)

	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	defer sdl.Quit()

	sdl.SetHint(sdl.HINT_RENDER_VSYNC, "1")
	sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "1")
	window, renderer, err := sdl.CreateWindowAndRenderer(screenWidth, screenHeight, sdl.WINDOW_SHOWN)
	if err != nil {
		panic(err)
	}
	defer window.Destroy()
	defer renderer.Destroy() // Want the renderer to be destroyed before the window.

	window.SetTitle(astroids.ScreenTitle)

	// load Textures

	//TODO: How do you defer thiis?
	textures[astroids.SPACESHIP] = LoadTexture(renderer, astroids.GD_ASSETS_PATH+astroids.SPACESHIP)
	textures[astroids.BULLET] = LoadTexture(renderer, astroids.GD_ASSETS_PATH+astroids.BULLET)
	textures[astroids.ROCK] = LoadTexture(renderer, astroids.GD_ASSETS_PATH+astroids.ROCK)
	textures[astroids.EXPLOSION] = LoadTexture(renderer, astroids.GD_ASSETS_PATH+astroids.EXPLOSION)

	entities := list.New()
	entities.PushBack(astroids.NewSpaceShipEntity(float32(screenWidth/2), float32(screenHeight/2)))

	var rockCount int32 = 0
	const maxRocks = 5
	var elapsedTime = float32(1 / 60.0)
	running := true
	var tick = sdl.GetTicks()
	for running {
		lastTick := tick
		tick = sdl.GetTicks()
		frameStart := time.Now()
		elapsedTick := tick - lastTick
		_ = elapsedTick

		// SDL can have a lot of events to process which is why we are checking if we are still running within the event loop
		for event := sdl.PollEvent(); event != nil && running; event = sdl.PollEvent() {
			switch t := event.(type) { //NOTE: this is a way to get type information in 'go', this event is bascially an interface.
			case *sdl.KeyboardEvent:
				if t.Repeat == 0 {
					if t.Type == sdl.KEYDOWN {
						switch t.Keysym.Sym {
						case sdl.K_ESCAPE:
							running = false
							break
						case sdl.K_SPACE:
							spaceShip, ok := entities.Front().Value.(*astroids.Spaceship)
							if ok {
								posOri := spaceShip.Ent.GetOrigin()
								angle := spaceShip.Ent.GetDegree()
								{
									//Left
									var xpos = posOri.X - 30*float32(math.Cos(angle*(math.Pi/180)))
									var ypos = posOri.Y - 30*float32(math.Sin(angle*(math.Pi/180)))
									entities.PushBack(astroids.NewBulletEntity(xpos, ypos, angle))
								}
								{
									//Right
									var xpos = posOri.X + 30*float32(math.Cos(angle*(math.Pi/180)))
									var ypos = posOri.Y + 30*float32(math.Sin(angle*(math.Pi/180)))
									entities.PushBack(astroids.NewBulletEntity(xpos, ypos, angle))
								}
							}
							break
						}

					}
				}
			case *sdl.QuitEvent:
				running = false
				break
			}
		}
		//
		// Update
		//
		if rockCount < maxRocks && rand.Int()%250 == 0 {
			rockCount++
			xpos := float32(rand.Intn(int(astroids.ScreenWidth)))
			ypos := float32(rand.Intn(int(astroids.ScreenHeight)))
			dxpos := float32(rand.Int() % 8)
			dypos := float32(rand.Int() % 8)
			angle := 0.0 // not used
			entities.PushBack(astroids.NewRockEntity(xpos, ypos, dxpos, dypos, angle))
		}

		// This is slow way to check for collisions
		for e := entities.Front(); e != nil; e = e.Next() {
			for ee := entities.Front(); ee != nil; ee = ee.Next() {
				if player, ok := e.Value.(*astroids.Spaceship); ok {
					if rock, ok := ee.Value.(*astroids.Rock); ok {
						// if the rock coliides with the player, reset the player
						if player.Ent.CollidesWith(&rock.Ent) {
							player.ApplyLife(-100)
							rock.ApplyLife(-100)
						}

					}
				}
				if bullet, ok := e.Value.(*astroids.Bullet); ok {
					if rock, ok := ee.Value.(*astroids.Rock); ok {
						// if the rock coliides with the player, reset the player
						if bullet.Ent.CollidesWith(&rock.Ent) {
							bullet.ApplyLife(-100)
							rock.ApplyLife(-100)
						}
					}
				}
			}
		}
		for e := entities.Front(); e != nil; {

			e.Value.(astroids.IEntity).Update(elapsedTime)
			if !e.Value.(astroids.IEntity).IsAlive() {
				if player, ok := e.Value.(*astroids.Spaceship); ok {
					player.Reset(float32(screenWidth/2), float32(screenHeight/2))
					e = e.Next()
				} else {
					toRemove := e
					e = e.Next()
					if e != nil {
						toRemove = e.Prev()
					}

					if rock, ok := toRemove.Value.(*astroids.Rock); ok {
						entities.PushBack(astroids.NewExplosionEntity(rock.Ent.GetPosition().X, rock.Ent.GetPosition().Y, 0, 0, 0))
						rockCount--
					}
					entities.Remove(toRemove)
				}
			} else {
				e = e.Next()
			}

		}

		//
		// Rendering
		//
		renderer.SetDrawColor(0, 0, 0, 255)
		renderer.Clear()
		for e := entities.Front(); e != nil; e = e.Next() {
			e.Value.(astroids.IEntity).Draw(renderer, textures)
		}
		renderer.Present()
		elapsedTime = float32(time.Since(frameStart).Seconds())

	}

	//TODO: will need to handle the case of cleanup when the application crashes
	// is there any overhead to keep deferring these.. hopefully go uses a stack based solution?
	for _, element := range textures {
		element.Destroy()
	}

}
